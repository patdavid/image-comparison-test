#!/bin/bash

ORIGINAL_WIDTH=1280
ORIGINAL_HEIGHT=650

CROP_WIDTH=1182
CROP_HEIGHT=412
CROP_XOFF=97
CROP_YOFF=191

BASE="base-negative.png"

echo original-width: $ORIGINAL_WIDTH

convert $BASE -crop ${CROP_WIDTH}x${CROP_HEIGHT}+${CROP_XOFF}+${CROP_YOFF} +repage base-negative-crop.png; 

for i in $( ls *.JPG ); do  
    echo Processing: $i;
	/cygdrive/f/Personal/Software/Hugin2015/bin/align_image_stack.exe -m -a TMP base-negative.png $i;
	convert TMP0001.tif -crop ${CROP_WIDTH}x${CROP_HEIGHT}+${CROP_XOFF}+${CROP_YOFF} +repage TMP.tif; 
	compare -metric AE -fuzz 15% base-negative-crop.png TMP.tif $i.png 2>>out.txt; 
	convert $i.png -background black -extent ${ORIGINAL_WIDTH}x${ORIGINAL_HEIGHT}-${CROP_XOFF}-${CROP_YOFF} $i.png; 
	echo -e " "$i"\r" >> out.txt ;
done #2>> out.txt
