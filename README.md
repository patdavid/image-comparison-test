# Image Comparison Test

A rather clunky implementation of a script for detecting changes in an image.

## Requirements

### Windows
On Windows this will require:

* [Cygwin](https://www.cygwin.com/)
* [Imagemagick](http://imagemagick.org/script/index.php)
* align_image_stack from [Hugin](http://hugin.sourceforge.net/)
